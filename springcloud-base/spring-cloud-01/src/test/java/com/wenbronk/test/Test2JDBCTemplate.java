package com.wenbronk.test;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.wenbronk.springboot.App;
import com.wenbronk.springboot.jpa.entity.Cat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=App.class)
public class Test2JDBCTemplate {

	
	/**
	 * 项目中有jpa的依赖, 不需要引入jdbc的依赖
	 */
	@Resource
	private JdbcTemplate jdbcTemplate;
	
	@Test
	public void test1() {
		String sql = "select * from cat where id = ?";
		Cat cat = jdbcTemplate.queryForObject(sql, new Object[]{2}, new BeanPropertyRowMapper<Cat>(Cat.class));
		System.out.println(cat.getCatName());
	}
	
}
