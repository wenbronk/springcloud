package com.wenbronk.test;

import javax.annotation.Resource;
import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.wenbronk.springboot.App;
import com.wenbronk.springboot.jpa.entity.Cat;
import com.wenbronk.springboot.jpa.repository.Cat2Repository;
import com.wenbronk.springboot.jpa.service.CatService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=App.class)
public class Test1Jpa {

	@Resource
	private CatService catService;
	
	@Inject
	private Cat2Repository cat2Repository;
	
	@Test
	public void test1()	{
		catService.delete(1);
	}
	
	/**
	 * 测试 方法名查询
	 */
	@Test
	public void test2() {
		Cat findByCatName = cat2Repository.findByCatName("tom");
		System.out.println(findByCatName.getCatName());
	}
	
	/**
	 * 测试自定义JPQL
	 */
	@Test
	public void test3() {
		Cat findFromCatName = cat2Repository.findFromCatName("tom");
		System.out.println(findFromCatName.getCatName());
	}
}
