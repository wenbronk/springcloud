package com.wenbronk.springboot.fastjson;

import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * springboot 集成 fastJson
 * @author root
 * @date 2017年5月13日
 */
public class Demo {

	private String name;
	
	/**可直接格式化*/
	@JSONField(format = "yyyy-MM-dd HH:mm")
	private Date date;
	
	/**可忽略不转换*/
	@JSONField(serialize = false)
	private String ignore;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getIgnore() {
		return ignore;
	}

	public void setIgnore(String ignore) {
		this.ignore = ignore;
	}
	
	
}
