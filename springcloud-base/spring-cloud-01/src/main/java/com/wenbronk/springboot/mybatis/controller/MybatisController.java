package com.wenbronk.springboot.mybatis.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.wenbronk.springboot.mybatis.entity.Demo;
import com.wenbronk.springboot.mybatis.service.DemoService;

/**
 * springboot 集成mybatis
 * 
 * @author root
 * @date 2017年5月13日
 */
@RestController
public class MybatisController {

	@Resource
	private DemoService demoService;
	
	@RequestMapping(value = "/query")
	public List<Demo> query() {
		List<Demo> findByName = demoService.findByName("tom");
		return findByName;
	}
	
	/**
	 * 分页
	 */
	@RequestMapping(value = "/queryByPage")
	public List<Demo> queryByPage() {
		// 第几页, 每页多少条
		PageHelper.offsetPage(1, 2);
		return demoService.findByName("tom");
	}
	
	/**
	 * 保存返回id
	 */
	@RequestMapping("/insert")
	public Demo save() {
		Demo demo = new Demo();
		demo.setName("tom");
		demo.setPassword("123");
		demoService.save(demo);
		return demo;
	}
	
}
