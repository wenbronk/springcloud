package com.wenbronk.springboot.jpa.repository;

import org.springframework.data.repository.CrudRepository;

import com.wenbronk.springboot.jpa.entity.Cat;

/**
 * repository接口
 * 不需要注解  @Repository
 * @author root
 * @date 2017年5月13日
 */
public interface CatRepository extends CrudRepository<Cat, Integer> {

}
