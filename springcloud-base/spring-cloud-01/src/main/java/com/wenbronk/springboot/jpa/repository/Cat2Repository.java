package com.wenbronk.springboot.jpa.repository;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import com.wenbronk.springboot.jpa.entity.Cat;

/**
 * 自定义repository
 * @author root
 * @date 2017年5月13日
 */
public interface Cat2Repository extends Repository<Cat, Integer>{

	/**
	 * 查询以find, get, query开头
	 */
	public Cat findByCatName(String catName);
	
	/**
	 * 编写 JPQL语句, 和HQL语句类似, 使用: 进行取值
	 */
	@Query("from Cat where catName = :cn")
	public Cat findFromCatName(@Param("cn") String catName);
}
