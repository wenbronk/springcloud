package com.wenbronk.springboot.jpa.controller;

import java.util.Iterator;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wenbronk.springboot.jpa.entity.Cat;
import com.wenbronk.springboot.jpa.service.CatService;

/**
 * 
 * @author root
 * @date 2017年5月13日
 */
@RestController
public class CatController {

	@Resource
	private CatService catService;
	
	@RequestMapping("/save")
	public String save() {
		Cat cat = new Cat();
		cat.setCatName("tom");
		cat.setCatAge(3);
		catService.save(cat);
		return "ok";
	}
	
	@RequestMapping("/find")
	public Cat find() {
		Iterable<Cat> findAll = catService.findAll();
		Iterator<Cat> iterator = findAll.iterator();
		return iterator.next();
	}
	
	@RequestMapping("/findOne/{id}")
	public Cat findOne(@PathVariable Integer id) {
		return catService.findOne(id);
	}
	
}
