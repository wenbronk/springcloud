package com.wenbronk.springboot.jpa.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 实体类
 * @Entity 生成表结构
 * @author wenbronk
 *
 */

@Entity
@Table(name="cat")	// 不写默认为cat
public class Cat {

	/**
	 * 指定主键及主键的生成策略
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String catName;
	private int catAge;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCatName() {
		return catName;
	}
	public void setCatName(String catName) {
		this.catName = catName;
	}
	public int getCatAge() {
		return catAge;
	}
	public void setCatAge(int catAge) {
		this.catAge = catAge;
	}
	
}
