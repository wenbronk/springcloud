package com.wenbronk.springboot.thymeleaf;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * thymeleaf 代替jsp
 * @author root
 * @date 2017年5月13日
 */

// 不可以加RestContoller, 否则全为json格式的了
@Controller
public class ThymeleafController {

	/**
	 * 
	 * @return
	 */
	@RequestMapping("/thymeleaf")
	public String thymeleaf() {
		return "hello";
	}
	
	
}
