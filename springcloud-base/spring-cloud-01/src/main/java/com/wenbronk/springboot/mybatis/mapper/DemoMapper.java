package com.wenbronk.springboot.mybatis.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import com.wenbronk.springboot.mybatis.entity.Demo;


/**
 * mapper
 * @author root
 * @date 2017年5月14日
 */
public interface DemoMapper {

	@Select("select * from Demo where name = #{name}")
	public List<Demo> getByName(String name);
	
	@Select("select * from Demo where id = #{id}")
	public Demo getById(Long id);
	
	@Select ("select name from Demo where id = #{id}")
	public String getNameById(Long id);
	
	/**
	 * 保存,获取子增长id
	 * @param demo
	 */
	@Insert("insert into Demo (name, password) values (#{name}, #{password})")
	@Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
	public long save(Demo demo);
	
}
