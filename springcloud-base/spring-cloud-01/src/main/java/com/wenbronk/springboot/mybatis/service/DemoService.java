package com.wenbronk.springboot.mybatis.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wenbronk.springboot.mybatis.entity.Demo;
import com.wenbronk.springboot.mybatis.mapper.DemoMapper;

@Service
public class DemoService {

	@Autowired
	private DemoMapper demoMapper;
	
	/**
	 * 分页
	 * @param name
	 * @return
	 */
	public List<Demo> findByName(String name) {
		return demoMapper.getByName(name);
	}
	
	/**
	 * 保存返回id
	 * @param demo
	 * @return
	 */
	@Transactional
	public long save(Demo demo) {
		return demoMapper.save(demo);
	}
	
}
