package com.wenbronk.springboot.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * springboot的全局异常处理
 * 1, 新建class 
 * 2, 添加注解 @ControllerAdvice
 * 3, 方法上注解; @ExceptionHandler
 * 4, 返回值是view, 方法的返回值是ModelAndView
 * 		返回值是String, 或json, 需要方法上添加@ResponseBody
 * @author root
 * @date 2017年5月13日
 */
@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(Exception.class)
	@ResponseBody
	public String defaultHandler() {
		return "your request error";
	}
	
	
}
