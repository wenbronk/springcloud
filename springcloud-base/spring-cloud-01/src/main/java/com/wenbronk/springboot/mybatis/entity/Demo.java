package com.wenbronk.springboot.mybatis.entity;

/**
 * mybatis的实体类
 * @author root
 * @date 2017年5月14日
 */
public class Demo {

	private long id;
	private String name;
	private String password;
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
