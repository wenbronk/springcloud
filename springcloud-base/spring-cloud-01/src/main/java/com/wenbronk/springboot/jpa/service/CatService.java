package com.wenbronk.springboot.jpa.service;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.wenbronk.springboot.jpa.entity.Cat;
import com.wenbronk.springboot.jpa.repository.CatRepository;

/**
 * 
 * @author root
 * @date 2017年5月13日
 */
@Service
public class CatService {

	@Inject
	private CatRepository catRepository;
	
	@Resource
	private JdbcTemplate jdbcTemplate;
	
	@Transactional
	public void save(Cat cat) {
		catRepository.save(cat);
	}
	
	@Transactional
	public void delete(Integer id) {
		catRepository.delete(id);
	}
	
	
	public Iterable<Cat> findAll() {
		return catRepository.findAll();
	}
	
	public Cat findOne(Integer id) {
		String sql = "select * from cat where id = ?";
		Cat cat = jdbcTemplate.queryForObject(sql, new Object[]{id}, new BeanPropertyRowMapper<Cat>(Cat.class));
		return cat;
	}
}
