package com.wenbronk.feign.without.hystrix.feign;

import com.wenbronk.feign.without.hystrix.entity.UserEntity;
import org.springframework.stereotype.Component;

/**
 * Created by root on 2017/5/30.
 */
@Component
public class UserFeingClientFallBack2 implements UserFeignClient2 {
    @Override
    public UserEntity findById(Long id) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(1L);
        return userEntity;
    }
}
