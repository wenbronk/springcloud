package com.wenbronk.feign.without.config;

import feign.Feign;
import feign.auth.BasicAuthRequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * feign 的自定义配置
 * 不可以和主类的componentScan冲突
 * Created by root on 2017/5/20.
 */
@Configuration
public class FeignConstomConfiguration2 {

    /**
     * 身份验证
     * @return
     */
    @Bean
    public BasicAuthRequestInterceptor basicAuthRequestInterceptor() {
        return new BasicAuthRequestInterceptor("wenbronk", "abc");
    }

    /**
     * 禁用hystrix
     */
    @Bean
    @Scope("prototype")
    public Feign.Builder feignBuilder() {
        return Feign.builder();
    }
}
