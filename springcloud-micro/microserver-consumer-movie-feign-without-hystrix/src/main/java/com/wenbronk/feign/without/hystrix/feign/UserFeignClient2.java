package com.wenbronk.feign.without.hystrix.feign;

import com.wenbronk.feign.without.config.FeignConstomConfiguration2;
import com.wenbronk.feign.without.hystrix.entity.UserEntity;
import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * Created by root on 2017/5/30.
 */
@FeignClient(value = "microservice-provider-user", configuration = FeignConstomConfiguration2.class, fallback = UserFeingClientFallBack2.class)
public interface UserFeignClient2 {

    /**
     * 因为使用的是feign默认的, 所以要使用feign的默认注解
     *
     * @param id
     * @return
     * @PathVariable 中 必须有value的值
     */
    @RequestLine("GET /simple/{id}")
    public UserEntity findById(@Param("id") Long id);

}
