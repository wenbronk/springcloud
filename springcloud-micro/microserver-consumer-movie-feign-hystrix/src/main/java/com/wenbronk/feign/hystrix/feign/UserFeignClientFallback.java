package com.wenbronk.feign.hystrix.feign;

import com.wenbronk.feign.hystrix.entity.UserEntity;
import org.springframework.stereotype.Component;

/**
 * 回滚类
 * Created by root on 2017/5/25.
 */
@Component
public class UserFeignClientFallback implements UserFeignClient {

    @Override
    public UserEntity findById(Long id) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(1L);
        return userEntity;
    }
}
