package com.wenbronk.feign.hystrix.controller;

import com.wenbronk.feign.hystrix.entity.UserEntity;
import com.wenbronk.feign.hystrix.feign.UserFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by root on 2017/5/25.
 */
@RestController
public class MovieController {

    @Autowired
    private UserFeignClient userFeignClient;

    @GetMapping("/simple/{id}")
    public UserEntity findUserById(@PathVariable Long id) {
        return userFeignClient.findById(id);
    }

}
