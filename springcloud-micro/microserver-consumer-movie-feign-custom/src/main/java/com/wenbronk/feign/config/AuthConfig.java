package com.wenbronk.feign.config;

import feign.auth.BasicAuthRequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 密码登录的config
 * Created by root on 2017/5/20.
 */
@Configuration
public class AuthConfig {
    @Bean
    public BasicAuthRequestInterceptor basicAuthRequestInterceptor() {
        return new BasicAuthRequestInterceptor("wenbronk", "abc");
    }
}
