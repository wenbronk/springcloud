package com.wenbronk.feign.customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * Created by root on 2017/5/20.
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
//@ComponentScan(excludeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION, value = {FeignAutoConfiguration.class})})
public class MovieFeignCustomApplication {
    public static void main(String[] args) {
        SpringApplication.run(MovieFeignCustomApplication.class, args);
    }
}
