package com.wenbronk.feign.customer.feignclient;

import com.wenbronk.feign.config.AuthConfig;
import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * 验证上一个注解, 只对他本身有效
 * 加上身份认证的信息
 * Created by root on 2017/5/20.
 */
@FeignClient(name = "microservice-provider-user", url = "http://localhost:8761/", configuration = AuthConfig.class)
public interface OtherFeignClient {

//    @RequestMapping(value = "eureka/apps/{serviceName}")
//    public String findServiceInfo(@PathVariable("serviceName") String serviceName);
    @RequestLine("GET eureka/apps/{serviceName}")
    public String findServiceInfo(@Param("serviceName") String serviceName);

}
