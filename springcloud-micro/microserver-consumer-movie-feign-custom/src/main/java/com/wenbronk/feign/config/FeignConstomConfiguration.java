package com.wenbronk.feign.config;

import feign.Contract;
import feign.Logger;
import feign.auth.BasicAuthRequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * feign 的自定义配置
 * 不可以和主类的componentScan冲突
 * Created by root on 2017/5/20.
 */
@Configuration
public class FeignConstomConfiguration {

    /**
     * 使用feign的默认注解
     * 所以不能使用GetMapping, RequestMapping那些...
     * @return
     */
    @Bean
    public Contract feignContract() {
        return new feign.Contract.Default();
    }

    /**
     * 身份验证
     * @return
     */
    @Bean
    public BasicAuthRequestInterceptor basicAuthRequestInterceptor() {
        return new BasicAuthRequestInterceptor("wenbronk", "abc");
    }

    /**
     * 在代码中更改日志的级别
     */
    @Configuration
    public class FooConfiguration {
        @Bean
        Logger.Level feignLoggerLevel() {
            return Logger.Level.FULL;
        }
    }
}
