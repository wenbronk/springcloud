package com.wenbronk.feign.customer.controller;

import com.wenbronk.feign.customer.entity.UserEntity;
import com.wenbronk.feign.customer.feignclient.OtherFeignClient;
import com.wenbronk.feign.customer.feignclient.UserFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by root on 2017/5/20.
 */
@RestController
public class MovieController {

    @Autowired
    private UserFeignClient userFeignClient;

    @Autowired
    private OtherFeignClient otherFeignClient;

    @GetMapping("/findUserById/{id}")
    public UserEntity findUserById(@PathVariable Long id) {
        return userFeignClient.findById(id);
    }

    @GetMapping("/{serviceName}")
    public String findUserInfo(@PathVariable String serviceName) {
        return otherFeignClient.findServiceInfo(serviceName);
    }

}
