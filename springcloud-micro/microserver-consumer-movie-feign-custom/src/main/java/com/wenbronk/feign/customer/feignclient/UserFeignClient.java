package com.wenbronk.feign.customer.feignclient;

import com.wenbronk.feign.config.FeignConstomConfiguration;
import com.wenbronk.feign.customer.entity.UserEntity;
import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * feign的接口
 * Created by root on 2017/5/20.
 */
@FeignClient(value = "microservice-provider-user", configuration = FeignConstomConfiguration.class)
public interface UserFeignClient {

    /**
     * 因为使用的是feign默认的, 所以要使用feign的默认注解
     *
     * @param id
     * @return
     * @PathVariable 中 必须有value的值
     */
    @RequestLine("GET /simple/{id}")
    public UserEntity findById(@Param("id") Long id);

}
