package com.wenbronk.provider.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Created by root on 2017/5/17.
 */
@SpringBootApplication
//@EnableEurekaClient     // 只对eureka 可用
@EnableDiscoveryClient  // 可用zk, eureka等
public class UserApplication {
    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class, args);
    }
}
