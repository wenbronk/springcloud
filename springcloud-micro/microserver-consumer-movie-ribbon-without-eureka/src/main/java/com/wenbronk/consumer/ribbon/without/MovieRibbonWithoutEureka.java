package com.wenbronk.consumer.ribbon.without;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * Created by root on 2017/5/20.
 */
@SpringBootApplication
@EnableEurekaClient
public class MovieRibbonWithoutEureka {
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    public static void main(String[] args) {
        SpringApplication.run(MovieRibbonWithoutEureka.class, args);
    }
}
