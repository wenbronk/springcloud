package com.wenbronk.consumer.feign.feignclient;

import com.wenbronk.consumer.feign.entity.UserEntity;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * feign的接口
 * Created by root on 2017/5/20.
 */
@FeignClient("microservice-provider-user")
public interface UserFeignClient {

    /**
     * 不可以使用 getMapping, 或者postMapping注解
     *
     * @param id
     * @return
     * @PathVariable 中 必须有value的值
     */
    @RequestMapping(value = "/simple/{id}", method = RequestMethod.GET)
    public UserEntity findById(@PathVariable("id") Long id);

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public UserEntity postUser(@RequestBody UserEntity userEntity);

    /**
     * 这儿不会管呗调用的用什么方法
     @PostMapping("/user")
     public User postUser(@RequestBody User user) {
        return user;
     }
     */
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public UserEntity getUser(UserEntity userEntity);

    /**
     * 如果被调用的方法是对象, 默认是post请求, 对方不可以是get请求
     // 该请求不会成功
     @GetMapping("/get-user")
     public User getUser(User user) {
         return user;
     }
     * @param userEntity
     * @return
     */
    @RequestMapping(value = "/get-user", method = RequestMethod.GET)
    public UserEntity getUserGet(UserEntity userEntity);




}
