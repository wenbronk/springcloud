package com.wenbronk.consumer.feign.controller;

import com.wenbronk.consumer.feign.entity.UserEntity;
import com.wenbronk.consumer.feign.feignclient.UserFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by root on 2017/5/20.
 */
@RestController
public class MovieController {

    @Autowired
    private UserFeignClient userFeignClient;

    @GetMapping("/findUserById/{id}")
    public UserEntity findUserById(@PathVariable Long id) {
        return userFeignClient.findById(id);
    }

    @PostMapping("/getUser")
    public UserEntity getUser(UserEntity user) {
//        return userFeignClient.postUser(user);
        return userFeignClient.getUser(user);
//        return userFeignClient.getUserGet(user);
    }

}
